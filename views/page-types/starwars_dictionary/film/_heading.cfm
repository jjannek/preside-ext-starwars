<cfparam name="args.title" />
<cfparam name="args.episode_id" />
<cfparam name="args.director" />
<cfparam name="args.producer" />
<cfparam name="args.release_date" />

<cfoutput>
    <h2>Episode #args.episode_id#: #args.title#</h2>

    <dl>
        <dt>Director</dt>
        <dd>#args.director#</dd>
        <dt>Producer</dt>
        <dd>#args.producer#</dd>
        <dt>Release Date</dt>
        <dd>#dateFormat( args.release_date, "full" )#</dd>
    </dl>
</cfoutput>