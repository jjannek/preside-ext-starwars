<cfparam name="args.vehicles" />
<cfparam name="args.title" default="Vehicles" />
<cfparam name="args.heading" default="h2" />

<cfoutput>
    <#args.heading#>#args.title#</#args.heading#>
    <ul>
        <cfloop query="args.vehicles">
            <li><a href="?display=vehicle&id=#id#">#Name#</a></li>
        </cfloop>
    </ul>
</cfoutput>