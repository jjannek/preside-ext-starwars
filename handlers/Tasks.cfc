component {

    property name="starwarsDataImport" inject="starwarsDataImport";
    property name="starwarsWookieTranslator" inject="starwarsWookieTranslator";

    /**
     * This is the task to import data from http://swapi.dev/api
     *
     * @schedule     disabled
     * @displayName  Star Wars data import
     * @displayGroup Star Wars
     *
     */
    private boolean function importStarWarsData( event, rc, prc, logger ) {
        return starwarsDataImport.import( arguments.logger ?: NullValue() );
    }

    /**
     * This is the task to translate Star Wars data to the Wookie language
     *
     * @schedule     disabled
     * @displayName  Star Wars Wookie Translate
     * @displayGroup Star Wars
     *
     */
    private boolean function translateStarWarsData( event, rc, prc, logger ) {
        return starwarsWookieTranslator.translate( arguments.logger ?: NullValue() );
    }
}