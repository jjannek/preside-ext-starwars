# Preside Extension: Star Wars

This is an extension for [Preside](https://www.presidecms.com) to import Star Wars data from http://swapi.co.

In detail the extension includes the following:

* Webservice Wrapper for http://swapi.co
* Preside Objects for Star Wars characters, films, vehicles, starships, species and planets
* Relations between the objects (e.g. which characters play in which film)
* Manual import task (via Task Manager) to fill all Star Wars data
* Custom Page type to display all the data
* Wookie Translator task (only works if multilingual feature is enabled and an additional language with the iso-code 'wk' is defined)

## Installation

Install the extension to your application via either of the methods detailed below (Git submodule / CommandBox) and then enable the extension by opening up the Preside developer console and entering:

    extension enable preside-ext-starwars
    reload all

### Git Submodule method

From the root of your application, type the following command:

    git submodule add https://bitbucket.org/jjannek/preside-ext-starwars.git application/extensions/preside-ext-starwars

### CommandBox (box.json) method

From the root of your application, type the following command:

    box install preside-ext-starwars

## Credits

This extension would not be possible without the excellent http://swapi.co.
The lookup table for the wookie translation is also based on their work. See https://github.com/phalt/swapi for details.

## Contribution

Feel free to fork and pull request. Any other feedback is also welcome - preferable on the PresideCMS slack channel.