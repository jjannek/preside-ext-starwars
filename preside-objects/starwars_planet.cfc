/**
 * @dataManagerGroup      starwars
 * @labelfield            name
 * @versioned             false
 * @multilingual          true
 * @datamanagergridfields name,diameter,rotation_period,orbital_period,gravity,population,climate,terrain,surface_water
 * @minimalgridfields     name
 */
component {
    property name="name"            type="string"  dbtype="varchar" maxlength="200" uniqueindexes="name" multilingual=true;
    property name="diameter"        type="string"  dbtype="varchar" maxlength="200";
    property name="rotation_period" type="string"  dbtype="varchar" maxlength="200";
    property name="orbital_period"  type="string"  dbtype="varchar" maxlength="200";
    property name="gravity"         type="string"  dbtype="varchar" maxlength="200";
    property name="population"      type="string"  dbtype="varchar" maxlength="200";
    property name="climate"         type="string"  dbtype="varchar" maxlength="200" multilingual=true;
    property name="terrain"         type="string"  dbtype="varchar" maxlength="200" multilingual=true;
    property name="surface_water"   type="string"  dbtype="varchar" maxlength="200";
    property name="residents" relationship="one-to-many"  relatedTo="starwars_character" relationshipKey="homeworld";
    property name="films"     relationship="many-to-many" relatedTo="starwars_film"      relatedVia="starwars_film_planet";
    property name="swapi_id"        type="numeric" dbtype="int"                     uniqueindexes="swapi_id";
}