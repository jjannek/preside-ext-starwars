/**
 * @singleton
 */
component {

// CONSTRUCTOR
    /**
     * @presideObjectService.inject presideObjectService
     * @swapi.inject swapi
     */
    public any function init( required any presideObjectService, required any swapi ) {

        _setPresideObjectService( arguments.presideObjectService );
        _setSwapi( arguments.swapi );

        return this;
    }

// PUBLIC FUNCTIONS
    public boolean function import( logger ) {
        var loggerAvailable = structKeyExists( arguments, "logger" );
        var canDebug        = loggerAvailable && arguments.logger.canDebug();
        var canError        = loggerAvailable && arguments.logger.canError();
        var canInfo         = loggerAvailable && arguments.logger.canInfo();

        if ( canInfo ) {
            arguments.logger.info( "Starting import of Star Wars data from swapi.dev..." );
        }

        var films = _getSwapi().getFilms();
        var filmMappings = _syncFilms( films, arguments.logger ?: NullValue() );

        var planets = _getSwapi().getPlanets();
        var planetMappings = _syncPlanets( planets, arguments.logger ?: NullValue() );

        var vehicles = _getSwapi().getVehicles();
        var vehicleMappings = _syncVehicles( vehicles, arguments.logger ?: NullValue() );

        var starships = _getSwapi().getStarships();
        var starshipMappings = _syncStarships( starships, arguments.logger ?: NullValue() );

        var characters = _getSwapi().getCharacters();

        characters = _mapHomeworld( characters, planetMappings );

        var characterMappings = _syncCharacters( characters, arguments.logger ?: NullValue() );

        var species = _getSwapi().getSpecies();

        species = _mapHomeworld( species, planetMappings );

        var speciesMappings = _syncSpecies( species, arguments.logger ?: NullValue() );

        _getPresideObjectService().deleteData( objectName="starwars_film_species"     , forceDeleteAll=true );
        _getPresideObjectService().deleteData( objectName="starwars_film_starship"    , forceDeleteAll=true );
        _getPresideObjectService().deleteData( objectName="starwars_film_vehicle"     , forceDeleteAll=true );
        _getPresideObjectService().deleteData( objectName="starwars_film_character"   , forceDeleteAll=true );
        _getPresideObjectService().deleteData( objectName="starwars_film_planet"      , forceDeleteAll=true );
        _getPresideObjectService().deleteData( objectName="starwars_character_species", forceDeleteAll=true );
        _getPresideObjectService().deleteData( objectName="starwars_starship_pilot"   , forceDeleteAll=true );
        _getPresideObjectService().deleteData( objectName="starwars_vehicle_pilot"    , forceDeleteAll=true );

        _syncReferences( objectName="starwars_film", records=films, recordMappings=filmMappings, property="species"   , propertyMappings=speciesMappings   );
        _syncReferences( objectName="starwars_film", records=films, recordMappings=filmMappings, property="starships" , propertyMappings=starshipMappings  );
        _syncReferences( objectName="starwars_film", records=films, recordMappings=filmMappings, property="vehicles"  , propertyMappings=vehicleMappings   );
        _syncReferences( objectName="starwars_film", records=films, recordMappings=filmMappings, property="characters", propertyMappings=characterMappings );
        _syncReferences( objectName="starwars_film", records=films, recordMappings=filmMappings, property="planets"   , propertyMappings=planetMappings    );
        
        _syncReferences( objectName="starwars_character", records=characters, recordMappings=characterMappings, property="species", propertyMappings=speciesMappings   );
        _syncReferences( objectName="starwars_starship" , records=starships , recordMappings=starshipMappings , property="pilots" , propertyMappings=characterMappings );
        _syncReferences( objectName="starwars_vehicle"  , records=vehicles  , recordMappings=vehicleMappings  , property="pilots" , propertyMappings=characterMappings );
        
        // TODO: maybe check if the reverse mappings need to be synced as well as there might be inconsistencies (missing records)

        if ( canInfo ) {
            arguments.logger.info( "Import of Star Wars data from swapi.dev completed." );
        }

        return true;
    }

// PRIVATE HELPERS
    private struct function _syncFilms( required array records, logger ) {
        return _syncRecords(
              objectName         = "starwars_film"
            , readableObjectName = "Star Wars Film"
            , labelField         = "title"
            , records            = arguments.records
            , keys               = [ "title", "episode_id", "opening_crawl", "director", "producer", "release_date", "swapi_id" ]
            , logger             = arguments.logger ?: NullValue()
        );
    }

    private struct function _syncPlanets( required array records, logger ) {
        return _syncRecords(
              objectName         = "starwars_planet"
            , readableObjectName = "Star Wars Planet"
            , records            = arguments.records
            , keys               = [ "name", "diameter", "rotation_period", "orbital_period", "gravity", "population", "climate", "terrain", "surface_water", "swapi_id" ]
            , logger             = arguments.logger ?: NullValue()
        );
    }

    private struct function _syncVehicles( required array records, logger ) {
        return _syncRecords(
              objectName         = "starwars_vehicle"
            , readableObjectName = "Star Wars Vehicle"
            , records            = arguments.records
            , keys               = [ "name", "model", "vehicle_class", "manufacturer", "length", "cost_in_credits", "crew", "passengers", "max_atmospheric_speed", "cargo_capacity", "consumables", "swapi_id" ]
            , logger             = arguments.logger ?: NullValue()
        );
    }

    private struct function _syncStarships( required array records, logger ) {
        return _syncRecords(
              objectName         = "starwars_starship"
            , readableObjectName = "Star Wars Starship"
            , records            = arguments.records
            , keys               = [ "name", "model", "starship_class", "manufacturer", "cost_in_credits", "length", "crew", "passengers", "max_atmospheric_speed", "hyperdrive_rating", "mglt", "cargo_capacity", "consumables", "swapi_id" ]
            , logger             = arguments.logger ?: NullValue()
        );
    }

    private struct function _syncCharacters( required array records, logger ) {
        return _syncRecords(
              objectName         = "starwars_character"
            , readableObjectName = "Star Wars Character"
            , records            = arguments.records
            , keys               = [ "name", "birth_year", "eye_color", "gender", "hair_color", "height", "mass", "skin_color", "homeworld", "swapi_id" ]
            , logger             = arguments.logger ?: NullValue()
        );
    }

    private struct function _syncSpecies( required array records, logger ) {
        return _syncRecords(
              objectName         = "starwars_species"
            , readableObjectName = "Star Wars Species"
            , records            = arguments.records
            , keys               = [ "name", "classification", "designation", "average_height", "average_lifespan", "eye_colors", "hair_colors", "skin_colors", "language", "homeworld", "swapi_id" ]
            , logger             = arguments.logger ?: NullValue()
        );
    }

    private struct function _syncRecords(
          required string objectName
        , required string readableObjectName
        , string labelField="name"
        , required array records
        , required array keys
        , logger
    ) {

        var loggerAvailable = structKeyExists( arguments, "logger" );
        var canDebug        = loggerAvailable && arguments.logger.canDebug();
        var canError        = loggerAvailable && arguments.logger.canError();
        var canInfo         = loggerAvailable && arguments.logger.canInfo();


        if ( canInfo ) {
            arguments.logger.info( "Now syncing '#arguments.readableObjectName#' records.");
        }

        var mappings = {};

        var validRecordIDs = [];
        var allExistingRecords = _getPresideObjectService().selectData( objectName=arguments.objectName );

        // insert/update
        for ( var record in arguments.records ) {
            var existingRecord = _getPresideObjectService().selectData( objectName=arguments.objectName, filter={ swapi_id=record.swapi_id } );
            var data = {};
            for ( var key in arguments.keys ) {
                if ( record.keyExists( key ) ) {
                    data[ key ] = record[ key ];
                }
            }
            if ( existingRecord.recordCount ) {
                mappings[ record.swapi_id ] = existingRecord.id;
                _getPresideObjectService().updateData(
                      objectName = arguments.objectName
                    , id         = existingRecord.id
                    , data       = data
                );
            }
            else {
                mappings[ record.swapi_id ] = _getPresideObjectService().insertData(
                      objectName = arguments.objectName
                    , data       = data
                );
            }
            validRecordIDs.append( mappings[ record.swapi_id ] );

            if ( canDebug && record.keyExists( arguments.labelField ) ) {
                arguments.logger.debug( "Synced '#record[ arguments.labelField ]#'.");
            }
        }

        // remove obsolete
        loop query="allExistingRecords" {
            if ( validRecordIDs.findNoCase( allExistingRecords.id ) eq 0 ) {
                _getPresideObjectService().deleteData( objectName=arguments.objectName, id=allExistingRecords.id );
            }
        }

        if ( canInfo ) {
            arguments.logger.info( "Syncing of '#arguments.readableObjectName#' records completed.");
        }

        return mappings;
    }

    private array function _mapHomeworld( required array records, required struct planetMappings ) {
            
        for ( var record in arguments.records ) {
            if ( !record.keyExists( "homeworld" ) ) {
                continue;
            }
            if ( arguments.planetMappings.keyExists( record.homeworld ) ) {
                record.homeworld = arguments.planetMappings[ record.homeworld ];
            }
            else {
                // TODO: invalid data, maybe log or throw an error
                record.delete( "homeworld" );
            }
        }

        return arguments.records;
    }

    private void function _syncReferences(
          required string objectName
        , required array records
        , required struct recordMappings
        , required string property
        , required struct propertyMappings
    ) {
        for ( var record in arguments.records ) {
            if ( !arguments.recordMappings.keyExists( record.swapi_id ) ) {
                continue;
            }
            if ( !record.keyExists( arguments.property ) ) {
                continue;
            }
            if ( !isArray( record[ arguments.property ] ) ) {
                continue;
            }

            var targetIds = [];

            for ( var id in record[ arguments.property ] ) {
                if ( arguments.propertyMappings.keyExists( id ) ) {
                    targetIds.append( arguments.propertyMappings[ id ] );
                }
            }

            if ( !isEmpty( targetIds ) ) {
                _getPresideObjectService().syncManyToManyData(
                      sourceObject   = arguments.objectName
                    , sourceProperty = arguments.property
                    , sourceId       = arguments.recordMappings[ record.swapi_id ]
                    , targetIdList   = targetIds.toList()
                );
            }
        }
    }

// GETTERS AND SETTERS
    private any function _getPresideObjectService() {
        return _presideObjectService;
    }
    private void function _setPresideObjectService( required any presideObjectService ) {
        _presideObjectService = arguments.presideObjectService;
    }

    private any function _getSwapi() {
        return _swapi;
    }
    private void function _setSwapi( required any swapi ) {
        _swapi = arguments.swapi;
    }
}