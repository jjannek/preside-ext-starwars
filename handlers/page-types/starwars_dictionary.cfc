component {

	private function index( event, rc, prc, args={} ) {
		return renderView(
			  view          = 'page-types/starwars_dictionary/index'
			, presideObject = 'starwars_dictionary'
			, id            = event.getCurrentPageId()
			, args          = args
		);
	}
}
