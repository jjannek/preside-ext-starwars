<cfparam name="args.species" />
<cfparam name="args.title" default="Species" />
<cfparam name="args.heading" default="h2" />

<cfoutput>
    <#args.heading#>#args.title#</#args.heading#>
    <ul>
        <cfloop query="args.species">
            <li><a href="?display=species&id=#id#">#Name#</a></li>
        </cfloop>
    </ul>
</cfoutput>