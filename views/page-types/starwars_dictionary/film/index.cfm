<cfparam name="args.title" />
<cfparam name="args.episode_id" />
<cfparam name="args.opening_crawl" />
<cfparam name="args.director" />
<cfparam name="args.producer" />
<cfparam name="args.release_date" />
<cfparam name="args.species" />
<cfparam name="args.starships" />
<cfparam name="args.vehicles" />
<cfparam name="args.characters" />
<cfparam name="args.planets" />

<cfoutput>
    #renderView( view="page-types/starwars_dictionary/film/_heading"     , args=args )#
    #renderView( view="page-types/starwars_dictionary/film/_openingCrawl", args=args )#

    #renderView( view="page-types/starwars_dictionary/species/listing"  , args={ species    = args.species   , heading="h3" } )#
    #renderView( view="page-types/starwars_dictionary/character/listing", args={ characters = args.characters, heading="h3" } )#
    #renderView( view="page-types/starwars_dictionary/planet/listing"   , args={ planets    = args.planets   , heading="h3" } )#
    #renderView( view="page-types/starwars_dictionary/starship/listing" , args={ starships  = args.starships , heading="h3" } )#
    #renderView( view="page-types/starwars_dictionary/vehicle/listing"  , args={ vehicles   = args.vehicles  , heading="h3" } )#
</cfoutput>