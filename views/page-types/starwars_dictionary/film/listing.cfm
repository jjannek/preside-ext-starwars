<cfparam name="args.films" />
<cfparam name="args.title" default="Films" />
<cfparam name="args.heading" default="h2" />

<cfoutput>
    <#args.heading#>#args.title#</#args.heading#>
    <ul>
        <cfloop query="args.films">
            <li><a href="?display=film&id=#id#">Episode #episode_id#: #title#</a></li>
        </cfloop>
    </ul>
</cfoutput>