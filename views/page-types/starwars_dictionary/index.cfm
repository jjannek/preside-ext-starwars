<cf_presideparam name="args.title"        type="string" field="page.title"        editable="true" />
<cf_presideparam name="args.main_content" type="string" field="page.main_content" editable="true" />

<cfparam name="rc.display" default="" />

<cfoutput>
    <div class="contents" >
        <div class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>#args.title#</h2>
                        <p>#args.main_content#</p>

                        <a href="?display=films">All Films</a>&nbsp;|&nbsp;
                        <a href="?display=planets">All Planets</a>&nbsp;|&nbsp;
                        <a href="?display=allspecies">All Species</a>&nbsp;|&nbsp;
                        <a href="?display=characters">All Characters</a>&nbsp;|&nbsp;
                        <a href="?display=starships">All Starships</a>&nbsp;|&nbsp;
                        <a href="?display=vehicles">All Vehicles</a>

                        <br><br>

                        <cfif [ "films", "planets", "characters", "allspecies", "starships", "vehicles" ].findNoCase( rc.display ) gt 0>
                            #renderViewlet( event="starwars.#rc.display#" )#
                        <cfelseif [ "film", "planet", "character", "species", "starship", "vehicle" ].findNoCase( rc.display ) gt 0>
                            #renderViewlet( event="starwars.#rc.display#", args={ id=rc.id } )#
                        </cfif>

                    </div>
                </div>
            </div>
        </div>
    </div>
</cfoutput>