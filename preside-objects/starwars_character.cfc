/**
 * @dataManagerGroup      starwars
 * @labelfield            name
 * @versioned             false
 * @multilingual          true
 * @datamanagergridfields name,birth_year,eye_color,gender,hair_color,height,mass,skin_color,homeworld
 * @minimalgridfields     name,homeworld
 */
component {
    property name="name"       type="string"  dbtype="varchar" maxlength="200" uniqueindexes="name" multilingual=true;
    property name="birth_year" type="string"  dbtype="varchar" maxlength="50";
    property name="eye_color"  type="string"  dbtype="varchar" maxlength="50" multilingual=true;
    property name="gender"     type="string"  dbtype="varchar" maxlength="30" multilingual=true;
    property name="hair_color" type="string"  dbtype="varchar" maxlength="40" multilingual=true;
    property name="height"     type="string"  dbtype="varchar" maxlength="20";
    property name="mass"       type="string"  dbtype="varchar" maxlength="20";
    property name="skin_color" type="string"  dbtype="varchar" maxlength="40" multilingual=true;
    property name="homeworld"  relationship="many-to-one"  relatedTo="starwars_planet";
    property name="films"      relationship="many-to-many" relatedTo="starwars_film"     relatedVia="starwars_film_character";
    property name="species"    relationship="many-to-many" relatedTo="starwars_species"  relatedVia="starwars_character_species";
    property name="starships"  relationship="many-to-many" relatedTo="starwars_starship" relatedVia="starwars_starship_pilot";
    property name="vehicles"   relationship="many-to-many" relatedTo="starwars_vehicle"  relatedVia="starwars_vehicle_pilot";
    property name="swapi_id"   type="numeric" dbtype="int"                     uniqueindexes="swapi_id";
}