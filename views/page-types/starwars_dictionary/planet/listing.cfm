<cfparam name="args.planets" />
<cfparam name="args.title" default="Planets" />
<cfparam name="args.heading" default="h2" />

<cfoutput>
    <#args.heading#>#args.title#</#args.heading#>
    <ul>
        <cfloop query="args.planets">
            <li><a href="?display=planet&id=#id#">#Name#</a></li>
        </cfloop>
    </ul>
</cfoutput>