<cfparam name="args.name" />
<cfparam name="args.diameter" />
<cfparam name="args.rotation_period" />
<cfparam name="args.orbital_period" />
<cfparam name="args.gravity" />
<cfparam name="args.population" />
<cfparam name="args.climate" />
<cfparam name="args.terrain" />
<cfparam name="args.surface_water" />
<cfparam name="args.residents" />
<cfparam name="args.films" />

<cfoutput>
    #renderView( view="page-types/starwars_dictionary/planet/_heading"  , args=args )#

    #renderView( view="page-types/starwars_dictionary/character/listing", args={ characters = args.residents , heading="h3", title="Residents" } )#
    #renderView( view="page-types/starwars_dictionary/film/listing"     , args={ films      = args.films     , heading="h3" } )#
</cfoutput>