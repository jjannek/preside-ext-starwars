<cfparam name="args.name" />
<cfparam name="args.diameter" />
<cfparam name="args.rotation_period" />
<cfparam name="args.orbital_period" />
<cfparam name="args.gravity" />
<cfparam name="args.population" />
<cfparam name="args.climate" />
<cfparam name="args.terrain" />
<cfparam name="args.surface_water" />

<cfoutput>
    <h2>#args.name#</h2>

    <dl>
        <dt>Diameter</dt>
        <dd>#args.diameter#</dd>
        <cfif isNumeric( args.rotation_period ) and args.rotation_period gt 0>
            <dt>Rotation Period</dt>
            <dd>#args.rotation_period# standard hours</dd>
        </cfif>
        <cfif isNumeric( args.orbital_period ) and args.orbital_period gt 0>
            <dt>Orbital Period</dt>
            <dd>#args.orbital_period# standard days</dd>
        </cfif>
        <dt>Gravity</dt>
        <dd>#args.gravity#</dd>
        <dt>Population</dt>
        <dd>#args.population#</dd>
        <dt>Climate</dt>
        <dd>#args.climate#</dd>
        <dt>Terrain</dt>
        <dd>#args.terrain#</dd>
        <cfif isNumeric( args.surface_water ) and args.surface_water gte 0>
            <dt>Surface_water</dt>
            <dd>#args.surface_water#%</dd>
        </cfif>
    </dl>
</cfoutput>