/**
 * @dataManagerGroup      starwars
 * @labelfield            title
 * @versioned             false
 * @multilingual          true
 * @datamanagergridfields title,episode_id,director,producer,release_date
 * @minimalgridfields     title,episode_id,release_date
 */
component {
    property name="title"         type="string"  dbtype="varchar" maxlength="200" uniqueindexes="title" multilingual=true;
    property name="episode_id"    type="numeric" dbtype="int"                     uniqueindexes="episode_id";
    property name="opening_crawl" type="string"  dbtype="text" multilingual=true;
    property name="director"      type="string"  dbtype="varchar" maxlength="200" multilingual=true;
    property name="producer"      type="string"  dbtype="varchar" maxlength="400" multilingual=true;
    property name="release_date"  type="date"    dbtype="date";
    property name="species"       relationship="many-to-many" relatedTo="starwars_species"   relatedVia="starwars_film_species";
    property name="starships"     relationship="many-to-many" relatedTo="starwars_starship"  relatedVia="starwars_film_starship";
    property name="vehicles"      relationship="many-to-many" relatedTo="starwars_vehicle"   relatedVia="starwars_film_vehicle";
    property name="characters"    relationship="many-to-many" relatedTo="starwars_character" relatedVia="starwars_film_character";
    property name="planets"       relationship="many-to-many" relatedTo="starwars_planet"    relatedVia="starwars_film_planet";
    property name="swapi_id"      type="numeric" dbtype="int"                     uniqueindexes="swapi_id";
}