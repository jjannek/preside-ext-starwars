<cfparam name="args.characters" />
<cfparam name="args.title" default="Characters" />
<cfparam name="args.heading" default="h2" />

<cfoutput>
    <#args.heading#>#args.title#</#args.heading#>
    <ul>
        <cfloop query="args.characters">
            <li><a href="?display=character&id=#id#">#Name#</a></li>
        </cfloop>
    </ul>
</cfoutput>