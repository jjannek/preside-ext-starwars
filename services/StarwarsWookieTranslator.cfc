/**
 * @singleton
 * @presideservice
 */
component {

// CONSTRUCTOR
    /**
     * @presideObjectService.inject presideObjectService
     * @multilingualPresideObjectService.inject multilingualPresideObjectService
     */
    public any function init( required any presideObjectService, required any multilingualPresideObjectService ) {

        _setPresideObjectService( arguments.presideObjectService );
        _setMultilingualPresideObjectService( arguments.multilingualPresideObjectService );

        // LOOKUP CREDITS: https://github.com/phalt/swapi
        _setWookieLookup({
            "a": "ra",
            "b": "rh",
            "c": "oa",
            "d": "wa",
            "e": "wo",
            "f": "ww",
            "g": "rr",
            "h": "ac",
            "i": "ah",
            "j": "sh",
            "k": "or",
            "l": "an",
            "m": "sc",
            "n": "wh",
            "o": "oo",
            "p": "ak",
            "q": "rq",
            "r": "rc",
            "s": "c",
            "t": "ao",
            "u": "hu",
            "v": "ho",
            "w": "oh",
            "x": "k",
            "y": "ro",
            "z": "uf"
        });

        return this;
    }

// PUBLIC FUNCTIONS
    public boolean function translate( logger ) {
        var loggerAvailable = structKeyExists( arguments, "logger" );
        var canDebug        = loggerAvailable && arguments.logger.canDebug();
        var canError        = loggerAvailable && arguments.logger.canError();
        var canInfo         = loggerAvailable && arguments.logger.canInfo();

        if ( canInfo ) {
            arguments.logger.info( "Starting translation of Star Wars data to wookie..." );
        }

        if ( !$isFeatureEnabled( "multilingual" ) ) {
            arguments.logger.error( "Wookie translation not possible because multilingual content feature is not enabled." );
            return false;
        }

        var languages = _getMultilingualPresideObjectService().listLanguages( includeDefault=false );
        var languageId = "";

        for ( var language in languages ) {
            if ( language.iso_code == "wk" ) {
                languageId = language.id;
                break;
            }
        }

        if ( isEmpty( languageId ) ) {
            if ( canError ) {
                arguments.logger.error( "Wookie language (ISO code 'wk') not found. No translation possible." );
            }
            return false;
        }

        var objectNames = [ "starwars_film", "starwars_character", "starwars_planet", "starwars_species", "starwars_starship", "starwars_vehicle" ];

        for ( var objectName in objectNames ) {
            var properties = _getPresideObjectService().getObjectProperties( objectName );

            var multilingualProperties = [];

            for ( var property in properties ) {
                if ( _getMultilingualPresideObjectService().isMultilingual( objectName, property ) ) {
                    multilingualProperties.append( property );
                }
            }

            var records = _getPresideObjectService().selectData( objectName=objectName );

            if ( canDebug ) {
                arguments.logger.debug( "Translating #records.recordCount# '#objectName#' records (#multilingualProperties.toList(', ')#)" );
            }

            for ( var record in records ) {
                var data = { _translation_active=true };

                for ( var propertyName in multilingualProperties ) {
                    data[ propertyName ] = _wookie( record[ propertyName ] );
                }

                _getMultilingualPresideObjectService().saveTranslation(
                    objectname=objectName,
                    id=record.id,
                    languageId=languageId,
                    data=data
                );
            }
        }

        if ( canInfo ) {
            arguments.logger.info( "Translation of Star Wars data to wookie completed." );
        }

        return true;
    }

// PRIVATE HELPERS
    private string function _wookie( required string input ) {

        if ( isEmpty( arguments.input ) ) {
            return "";
        }

        var result = "";
        var char = "";
        var wookieLookup = _getWookieLookup();

        for ( var i = 1; i <= len( arguments.input ); i++ ) {
            char = mid( arguments.input, i, 1 );
            result &= wookieLookup.keyExists( char ) ? wookieLookup[ char ] : char;
        }

        return result;
    }

// GETTERS AND SETTERS
    private any function _getPresideObjectService() {
        return _presideObjectService;
    }
    private void function _setPresideObjectService( required any presideObjectService ) {
        _presideObjectService = arguments.presideObjectService;
    }

    private any function _getMultilingualPresideObjectService() {
        return _multilingualPresideObjectService;
    }
    private void function _setMultilingualPresideObjectService( required any multilingualPresideObjectService ) {
        _multilingualPresideObjectService = arguments.multilingualPresideObjectService;
    }

    private struct function _getWookieLookup() {
        return _wookieLookup;
    }
    private void function _setWookieLookup( required struct wookieLookup ) {
        _wookieLookup = arguments.wookieLookup;
    }
}