/**
 * @singleton
 */
component {

// CONSTRUCTOR
    public any function init(
          string  baseUrl  = "https://swapi.dev/api"
        , numeric httpTimeout  = 60
    ) {

        _setBaseUrl( arguments.baseUrl );
        _setHttpTimeout( arguments.httpTimeout );

        return this;
    }

// PUBLIC METHODS
    public array function getFilms() {
        return _pagedRestCall( endpoint="/films", detectReferencedIDs=[ "characters", "planets", "starships", "vehicles", "species" ] );
    }

    public array function getCharacters() {
        return _pagedRestCall( endpoint="/people", detectReferencedIDs=[ "homeworld", "films", "species", "vehicles", "starships" ] );   
    }

    public array function getVehicles() {
        return _pagedRestCall( endpoint="/vehicles", detectReferencedIDs=[ "pilots", "films" ] );
    }

    public array function getStarships() {
        return _pagedRestCall( endpoint="/starships", detectReferencedIDs=[ "pilots", "films" ] );
    }

    public array function getPlanets() {
        return _pagedRestCall( endpoint="/planets", detectReferencedIDs=[ "residents", "films" ] );
    }

    public array function getSpecies() {
        return _pagedRestCall( endpoint="/species", detectReferencedIDs=[ "homeworld", "people", "films" ] );
    }

// PRIVATE HELPERS
    private array function _pagedRestCall( required string endpoint, array detectReferencedIDs=[] ) {
        var result = [];

        var fullUrl = _getBaseUrl() & arguments.endpoint;

        while ( true ) {
            var restCallResult = _restCall( fullUrl );
            result.append( restCallResult.results, true );

            if ( restCallResult.keyExists( "next" ) && isSimpleValue( restCallResult.next ) && len( restCallResult.next ) && left( restCallResult.next, 4 ) == "http" ) {
                fullUrl = restCallResult.next;
            }
            else {
                break;
            }

        }

        for ( var item in result ) {
            item.delete( "created" );
            item.delete( "edited" );
            item.swapi_id = _getIdFromUrl( item.url );
            item.delete( "url" );
            for ( var key in arguments.detectReferencedIDs ) {
                if ( !item.keyExists( key ) ) {
                    continue;
                }
                
                if ( isSimpleValue( item[ key ] ) ) {
                    item[ key ] = _getIdFromUrl( item[ key ] );
                }
                else if ( isArray( item[ key ] ) ) {
                    var newReferencedCollection = [];
                    for ( var referencedUrl in item[ key ] ) {
                        newReferencedCollection.append( _getIdFromUrl( referencedUrl ) );
                    }
                    item[ key ] = newReferencedCollection;
                }
            }
        }

        return result;
    }

    private numeric function _getIdFromUrl( required string swapiUrl ) {

        // maybe remove trailing slash
        if ( right( arguments.swapiUrl, 1 ) == "/" ) {
            arguments.swapiUrl = left( arguments.swapiUrl, len( arguments.swapiUrl ) - 1 );
        }

        return listLast( arguments.swapiUrl, "/" );
    }

    private any function _restCall( required string fullUrl ) {

        var httpResult = "";

        http url       = arguments.fullUrl
             method    = "get"
             timeout   = _getHttpTimeout()
             result    = "httpResult" {}

        return _processApiResponse( argumentCollection=httpResult );
    }


    private any function _processApiResponse( string filecontent="", string status_code="" ) {

        if ( arguments.status_code neq 200 ) {
            throw(
                  type      = "swapi.unexpected"
                , message   = "An unexpected response was returned from the SWAPI server."
                , errorCode = Val( arguments.status_code ) ? arguments.status_code : 500
            );
        }
        
        try {
            return DeserializeJSON( arguments.fileContent );
        }
        catch ( any e ) {
            throw(
                  type    = "swapi.unexpected"
                , message = "Unexpected error processing SWAPI response. Response body: [#arguments.fileContent#]"
            );
        }
    }

// GETTERS AND SETTERS
    private string function _getBaseUrl() {
        return _baseUrl;
    }
    private void function _setBaseUrl( required string baseUrl ) {
        _baseUrl = arguments.baseUrl;
    }

    private numeric function _getHttpTimeout() {
        return _httpTimeout;
    }
    private void function _setHttpTimeout( required numeric httpTimeout ) {
        _httpTimeout = arguments.httpTimeout;
    }
}