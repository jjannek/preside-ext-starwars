<cfparam name="args.name" />
<cfparam name="args.birth_year" />
<cfparam name="args.eye_color" />
<cfparam name="args.gender" />
<cfparam name="args.hair_color" />
<cfparam name="args.height" />
<cfparam name="args.mass" />
<cfparam name="args.skin_color" />
<cfparam name="args.homeworldId" />
<cfparam name="args.homeworldName" />

<cfoutput>
    <h2>#args.name#</h2>

    <dl>
        <dt>Birth year</dt>
        <dd>#args.birth_year#</dd>
        <dt>Eye color</dt>
        <dd>#args.eye_color#</dd>
        <dt>gender</dt>
        <dd>#args.gender#</dd>
        <dt>Hair color</dt>
        <dd>#args.hair_color#</dd>
        <cfif isNumeric( args.height ) and args.height gte 0>
            <dt>Height</dt>
            <dd>#args.height# centimeters</dd>
        </cfif>
        <cfif isNumeric( args.mass ) and args.mass gte 0>
            <dt>Mass</dt>
            <dd>#args.mass# kilograms</dd>
        </cfif>
        <dt>Skin color</dt>
        <dd>#args.skin_color#</dd>
        <dt>Homeworld</dt>
        <dd><a href="?display=planet&id=#args.homeworldId#">#args.homeworldName#</a></dd>
    </dl>
</cfoutput>