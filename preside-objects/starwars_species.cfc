/**
 * @dataManagerGroup      starwars
 * @labelfield            name
 * @versioned             false
 * @multilingual          true
 * @datamanagergridfields name,classification,designation,average_height,average_lifespan,eye_colors,hair_colors,skin_colors,language,homeworld
 * @minimalgridfields     name,language,homeworld
 */
component {
	property name="name"             type="string"  dbtype="varchar" maxlength="200" uniqueindexes="name" multilingual=true;
    property name="classification"   type="string"  dbtype="varchar" maxlength="200" multilingual=true;
    property name="designation"      type="string"  dbtype="varchar" maxlength="200" multilingual=true;
    property name="average_height"   type="string"  dbtype="varchar" maxlength="200";
    property name="average_lifespan" type="string"  dbtype="varchar" maxlength="200";
    property name="eye_colors"       type="string"  dbtype="varchar" maxlength="200" multilingual=true;
    property name="hair_colors"      type="string"  dbtype="varchar" maxlength="200" multilingual=true;
    property name="skin_colors"      type="string"  dbtype="varchar" maxlength="200" multilingual=true;
    property name="language"         type="string"  dbtype="varchar" maxlength="200" multilingual=true;
    property name="homeworld" relationship="many-to-one"  relatedTo="starwars_planet";
    property name="people"    relationship="many-to-many" relatedTo="starwars_character" relatedVia="starwars_character_species";
	property name="films"     relationship="many-to-many" relatedTo="starwars_film"      relatedVia="starwars_film_species";
    property name="swapi_id"         type="numeric" dbtype="int"                     uniqueindexes="swapi_id";
}