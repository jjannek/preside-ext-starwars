<cfparam name="args.starships" />
<cfparam name="args.title" default="Starships" />
<cfparam name="args.heading" default="h2" />

<cfoutput>
    <#args.heading#>#args.title#</#args.heading#>
    <ul>
        <cfloop query="args.starships">
            <li><a href="?display=starship&id=#id#">#Name#</a></li>
        </cfloop>
    </ul>
</cfoutput>