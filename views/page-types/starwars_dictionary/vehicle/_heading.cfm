<cfparam name="args.name" />
<cfparam name="args.model" />
<cfparam name="args.vehicle_class" />
<cfparam name="args.manufacturer" />
<cfparam name="args.length" />
<cfparam name="args.cost_in_credits" />
<cfparam name="args.crew" />
<cfparam name="args.passengers" />
<cfparam name="args.max_atmospheric_speed" />
<cfparam name="args.cargo_capacity" />
<cfparam name="args.consumables" />

<cfoutput>
    <h2>#args.name#</h2>

    <dl>
        <dt>Model</dt>
        <dd>#args.model#</dd>
        <dt>Vehicle class</dt>
        <dd>#args.vehicle_class#</dd>
        <dt>Manufacturer</dt>
        <dd>#args.manufacturer#</dd>
        <cfif isNumeric( args.length ) and args.length gt 0>
            <dt>Length</dt>
            <dd>#args.length# meters</dd>
        </cfif>
        <cfif isNumeric( args.cost_in_credits ) and args.cost_in_credits gt 0>
            <dt>Cost</dt>
            <dd>#args.cost_in_credits# galactic credits</dd>
        </cfif>
        <dt>Crew</dt>
        <dd>#args.crew#</dd>
        <dt>Passengers</dt>
        <dd>#args.passengers#</dd>
        <cfif ( isNumeric( args.max_atmospheric_speed ) and args.max_atmospheric_speed gt 0 ) or args.max_atmospheric_speed eq "N/A">
            <dt>Maximum atmospheric speed</dt>
            <dd>
                <cfif isNumeric( args.max_atmospheric_speed ) and args.max_atmospheric_speed gt 0>
                    #args.max_atmospheric_speed#
                <cfelse>
                    incapable of atmospheric flight
                </cfif>
            </dd>
        </cfif>
        <cfif isNumeric( args.cargo_capacity ) and args.cargo_capacity gt 0>
            <dt>Cargo capacity</dt>
            <dd>#args.cargo_capacity# kilograms</dd>
        </cfif>
        <cfif isNumeric( args.consumables ) and args.consumables gt 0>
            <dt>Consumables</dt>
            <dd>#args.consumables# days</dd>
        </cfif>
    </dl>
</cfoutput>