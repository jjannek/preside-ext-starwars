/**
 * @singleton
 */
component {

// CONSTRUCTOR
    /**
     * @presideObjectService.inject presideObjectService
     */
    public any function init( required any presideObjectService ) {

        _setPresideObjectService( arguments.presideObjectService );

        return this;
    }

// PUBLIC FUNCTIONS
    public query function getFilms() {
        return _getPresideObjectService().selectData(
              objectName   = "starwars_film"
            , selectFields = [ "id", "episode_id", "title" ]
            , orderBy      = "episode_id" 
        );
    }

    public struct function getFilm( required string id ) {
        var film        = _getSingleRecord( objectName="starwars_film", id=arguments.id );

        film.species    = _getFilmSpecies( arguments.id );
        film.starships  = _getFilmStarships( arguments.id );
        film.vehicles   = _getFilmVehicles( arguments.id );
        film.characters = _getFilmCharacters( arguments.id );
        film.planets    = _getFilmPlanets( arguments.id );

        return film;
    }

    public query function getPlanets() {
        return _getPresideObjectService().selectData(
              objectName   = "starwars_planet"
            , selectFields = [ "id", "name" ]
            , orderBy      = "name" 
        );
    }

    public struct function getPlanet( required string id ) {
        var planet       = _getSingleRecord( objectName="starwars_planet", id=arguments.id );

        planet.residents = _getPlanetResidents( arguments.id );
        planet.films     = _getPlanetFilms( arguments.id );

        return planet;
    }

    public query function getAllSpecies() {
        return _getPresideObjectService().selectData(
              objectName   = "starwars_species"
            , selectFields = [ "id", "name" ]
            , orderBy      = "name" 
        );
    }

    public struct function getSpecies( required string id ) {
        var species    = _getSingleRecord( objectName="starwars_species", id=arguments.id, selectFields=[ "id", "name", "classification", "designation", "average_height", "average_lifespan", "eye_colors", "hair_colors", "skin_colors", "language", "homeworld.id as homeworldId", "homeworld.name as homeworldName" ] );

        species.people = _getSpeciesPeople( arguments.id );
        species.films  = _getSpeciesFilms( arguments.id );

        return species;
    }

    public query function getCharacters() {
        return _getPresideObjectService().selectData(
              objectName   = "starwars_character"
            , selectFields = [ "id", "name" ]
            , orderBy      = "name" 
        );
    }

    public struct function getCharacter( required string id ) {
        var character       = _getSingleRecord( objectName="starwars_character", id=arguments.id, selectFields=[ "id", "name", "birth_year", "eye_color", "gender", "hair_color", "height", "mass", "skin_color", "homeworld.id as homeworldId", "homeworld.name as homeworldName" ] );

        character.films     = _getCharacterFilms( arguments.id );
        character.species   = _getCharacterSpecies( arguments.id );
        character.starships = _getCharacterStarships( arguments.id );
        character.vehicles  = _getCharacterVehicles( arguments.id );

        return character;
    }

    public query function getStarships() {
        return _getPresideObjectService().selectData(
              objectName   = "starwars_starship"
            , selectFields = [ "id", "name" ]
            , orderBy      = "name" 
        );
    }

    public struct function getStarship( required string id ) {
        var starship    = _getSingleRecord( objectName="starwars_starship", id=arguments.id );

        starship.films  = _getStarshipFilms( arguments.id );
        starship.pilots = _getStarshipPilots( arguments.id );

        return starship;
    }

    public query function getVehicles() {
        return _getPresideObjectService().selectData(
              objectName   = "starwars_vehicle"
            , selectFields = [ "id", "name" ]
            , orderBy      = "name" 
        );
    }

    public struct function getVehicle( required string id ) {
        var vehicle    = _getSingleRecord( objectName="starwars_vehicle", id=arguments.id );

        vehicle.films  = _getVehicleFilms( arguments.id );
        vehicle.pilots = _getVehiclePilots( arguments.id );

        return vehicle;
    }

// PRIVATE HELPERS
    private struct function _getSingleRecord( required string objectName, required string id, array selectFields=[] ) {
        var q = _getPresideObjectService().selectData( objectName=arguments.objectName, id=arguments.id, selectFields=arguments.selectFields );
        return q.recordCount ? q.rowData( 1 ) : {};   
    }

    private query function _getFilmSpecies( required string filmId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_film"
            , propertyName="species"
            , id=arguments.filmId
            , selectFields = [ "species.id", "species.name" ]
            , orderBy="species.name"
        );
    }

    private query function _getFilmStarships( required string filmId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_film"
            , propertyName="starships"
            , id=arguments.filmId
            , selectFields = [ "starships.id", "starships.name" ]
            , orderBy="starships.name"
        );
    }

    private query function _getFilmVehicles( required string filmId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_film"
            , propertyName="vehicles"
            , id=arguments.filmId
            , selectFields = [ "vehicles.id", "vehicles.name" ]
            , orderBy="vehicles.name"
        );
    }

    private query function _getFilmCharacters( required string filmId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_film"
            , propertyName="characters"
            , id=arguments.filmId
            , selectFields = [ "characters.id", "characters.name" ]
            , orderBy="characters.name"
        );
    }

    private query function _getFilmPlanets( required string filmId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_film"
            , propertyName="planets"
            , id=arguments.filmId
            , selectFields = [ "planets.id", "planets.name" ]
            , orderBy="planets.name"
        );
    }

    private query function _getPlanetResidents( required string planetId ) {
        return _getPresideObjectService().selectData(
              objectName="starwars_character"
            , filter={ homeworld=arguments.planetId }
            , selectFields = [ "id", "name" ]
            , orderBy="name"
        );
    }

    private query function _getPlanetFilms( required string planetId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_planet"
            , propertyName="films"
            , id=arguments.planetId
            , selectFields = [ "films.id", "films.episode_id", "films.title" ]
            , orderBy="films.episode_id"
        );
    }

    private query function _getSpeciesPeople( required string speciesId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_species"
            , propertyName="people"
            , id=arguments.speciesId
            , selectFields = [ "people.id", "people.name" ]
            , orderBy="people.name"
        );
    }

    private query function _getSpeciesFilms( required string speciesId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_species"
            , propertyName="films"
            , id=arguments.speciesId
            , selectFields = [ "films.id", "films.episode_id", "films.title" ]
            , orderBy="films.episode_id"
        );
    }

    private query function _getCharacterFilms( required string characterId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_character"
            , propertyName="films"
            , id=arguments.characterId
            , selectFields = [ "films.id", "films.episode_id", "films.title" ]
            , orderBy="films.episode_id"
        );
    }

    private query function _getCharacterSpecies( required string characterId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_character"
            , propertyName="species"
            , id=arguments.characterId
            , selectFields = [ "species.id", "species.name" ]
            , orderBy="species.name"
        );
    }

    private query function _getCharacterStarships( required string characterId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_character"
            , propertyName="starships"
            , id=arguments.characterId
            , selectFields = [ "starships.id", "starships.name" ]
            , orderBy="starships.name"
        );
    }

    private query function _getCharacterVehicles( required string characterId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_character"
            , propertyName="vehicles"
            , id=arguments.characterId
            , selectFields = [ "vehicles.id", "vehicles.name" ]
            , orderBy="vehicles.name"
        );
    }

    private query function _getStarshipFilms( required string starshipId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_starship"
            , propertyName="films"
            , id=arguments.starshipId
            , selectFields = [ "films.id", "films.episode_id", "films.title" ]
            , orderBy="films.episode_id"
        );
    }

    private query function _getStarshipPilots( required string starshipId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_starship"
            , propertyName="pilots"
            , id=arguments.starshipId
            , selectFields = [ "pilots.id", "pilots.name" ]
            , orderBy="pilots.name"
        );
    }

    private query function _getVehicleFilms( required string vehicleId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_vehicle"
            , propertyName="films"
            , id=arguments.vehicleId
            , selectFields = [ "films.id", "films.episode_id", "films.title" ]
            , orderBy="films.episode_id"
        );
    }

    private query function _getVehiclePilots( required string vehicleId ) {
        return _getPresideObjectService().selectManyToManyData(
              objectName="starwars_vehicle"
            , propertyName="pilots"
            , id=arguments.vehicleId
            , selectFields = [ "pilots.id", "pilots.name" ]
            , orderBy="pilots.name"
        );
    }

// GETTERS AND SETTERS
    private any function _getPresideObjectService() {
        return _presideObjectService;
    }
    private void function _setPresideObjectService( required any presideObjectService ) {
        _presideObjectService = arguments.presideObjectService;
    }
}