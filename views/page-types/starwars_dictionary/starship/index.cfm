<cfparam name="args.name" />
<cfparam name="args.model" />
<cfparam name="args.starship_class" />
<cfparam name="args.manufacturer" />
<cfparam name="args.cost_in_credits" />
<cfparam name="args.length" />
<cfparam name="args.crew" />
<cfparam name="args.passengers" />
<cfparam name="args.max_atmospheric_speed" />
<cfparam name="args.hyperdrive_rating" />
<cfparam name="args.mglt" />
<cfparam name="args.cargo_capacity" />
<cfparam name="args.consumables" />
<cfparam name="args.films" />
<cfparam name="args.pilots" />

<cfoutput>
    #renderView( view="page-types/starwars_dictionary/starship/_heading" , args=args )#

    #renderView( view="page-types/starwars_dictionary/film/listing"     , args={ films      = args.films , heading="h3" } )#
    #renderView( view="page-types/starwars_dictionary/character/listing", args={ characters = args.pilots, heading="h3", title="Pilots" } )#
</cfoutput>