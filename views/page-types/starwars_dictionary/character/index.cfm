<cfparam name="args.name" />
<cfparam name="args.birth_year" />
<cfparam name="args.eye_color" />
<cfparam name="args.gender" />
<cfparam name="args.hair_color" />
<cfparam name="args.height" />
<cfparam name="args.mass" />
<cfparam name="args.skin_color" />
<cfparam name="args.homeworldId" />
<cfparam name="args.homeworldName" />
<cfparam name="args.films" />
<cfparam name="args.species" />
<cfparam name="args.starships" />
<cfparam name="args.vehicles" />

<cfoutput>
    #renderView( view="page-types/starwars_dictionary/character/_heading"     , args=args )#

    #renderView( view="page-types/starwars_dictionary/film/listing"     , args={ films     = args.films     , heading="h3" } )#
    #renderView( view="page-types/starwars_dictionary/species/listing"  , args={ species   = args.species   , heading="h3" } )#
    #renderView( view="page-types/starwars_dictionary/starship/listing" , args={ starships = args.starships , heading="h3" } )#
    #renderView( view="page-types/starwars_dictionary/vehicle/listing"  , args={ vehicles  = args.vehicles  , heading="h3" } )#
</cfoutput>