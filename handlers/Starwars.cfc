component {

    property name="starwarsBrowser" inject="starwarsBrowser";

    private function films( event, rc, prc, args={} ) {
        args.films = starwarsBrowser.getFilms();
        return renderView( view='page-types/starwars_dictionary/film/listing', args=args );
    }

    private function film( event, rc, prc, args={} ) {
        args.append( starwarsBrowser.getFilm( id=args.id ) );
        return renderView( view='page-types/starwars_dictionary/film/index', args=args );
    }

    private function planets( event, rc, prc, args={} ) {
        args.planets = starwarsBrowser.getPlanets();
        return renderView( view='page-types/starwars_dictionary/planet/listing', args=args );
    }

    private function planet( event, rc, prc, args={} ) {
        args.append( starwarsBrowser.getPlanet( id=args.id ) );
        return renderView( view='page-types/starwars_dictionary/planet/index', args=args );
    }

    private function allspecies( event, rc, prc, args={} ) {
        args.species = starwarsBrowser.getAllSpecies();
        return renderView( view='page-types/starwars_dictionary/species/listing', args=args );
    }

    private function species( event, rc, prc, args={} ) {
        args.append( starwarsBrowser.getSpecies( id=args.id ) );
        return renderView( view='page-types/starwars_dictionary/species/index', args=args );
    }

    private function characters( event, rc, prc, args={} ) {
        args.characters = starwarsBrowser.getCharacters();
        return renderView( view='page-types/starwars_dictionary/character/listing', args=args );
    }

    private function character( event, rc, prc, args={} ) {
        args.append( starwarsBrowser.getCharacter( id=args.id ) );
        return renderView( view='page-types/starwars_dictionary/character/index', args=args );
    }

    private function starships( event, rc, prc, args={} ) {
        args.starships = starwarsBrowser.getStarships();
        return renderView( view='page-types/starwars_dictionary/starship/listing', args=args );
    }

    private function starship( event, rc, prc, args={} ) {
        args.append( starwarsBrowser.getStarship( id=args.id ) );
        return renderView( view='page-types/starwars_dictionary/starship/index', args=args );
    }

    private function vehicles( event, rc, prc, args={} ) {
        args.vehicles = starwarsBrowser.getVehicles();
        return renderView( view='page-types/starwars_dictionary/vehicle/listing', args=args );
    }

    private function vehicle( event, rc, prc, args={} ) {
        args.append( starwarsBrowser.getVehicle( id=args.id ) );
        return renderView( view='page-types/starwars_dictionary/vehicle/index', args=args );
    }
}