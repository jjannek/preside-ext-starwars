/**
 * @dataManagerGroup      starwars
 * @labelfield            name
 * @versioned             false
 * @multilingual          true
 * @datamanagergridfields name,model,starship_class,manufacturer,cost_in_credits,length,crew,passengers,max_atmospheric_speed,hyperdrive_rating,mglt,cargo_capacity,consumables
 * @minimalgridfields     name,model,starship_class
 */
component {
	property name="name"                  type="string"  dbtype="varchar" maxlength="200" uniqueindexes="name" multilingual=true;
	property name="model"                 type="string"  dbtype="varchar" maxlength="400" multilingual=true;
	property name="starship_class"        type="string"  dbtype="varchar" maxlength="400" multilingual=true;
	property name="manufacturer"          type="string"  dbtype="varchar" maxlength="400" multilingual=true;
	property name="cost_in_credits"       type="string"  dbtype="varchar" maxlength="200";
	property name="length"                type="string"  dbtype="varchar" maxlength="200";
	property name="crew"                  type="string"  dbtype="varchar" maxlength="200";
	property name="passengers"            type="string"  dbtype="varchar" maxlength="200";
    property name="max_atmospheric_speed" type="string"  dbtype="varchar" maxlength="200";
    property name="hyperdrive_rating"     type="string"  dbtype="varchar" maxlength="200";
    property name="mglt"                  type="string"  dbtype="varchar" maxlength="200";
    property name="cargo_capacity"        type="string"  dbtype="varchar" maxlength="200";
    property name="consumables"           type="string"  dbtype="varchar" maxlength="200" multilingual=true;
    property name="films"  relationship="many-to-many" relatedTo="starwars_film"       relatedVia="starwars_film_starship";
    property name="pilots" relationship="many-to-many" relatedTo="starwars_character"  relatedVia="starwars_starship_pilot";
    property name="swapi_id"              type="numeric" dbtype="int"                     uniqueindexes="swapi_id";
}