<cfparam name="args.name" />
<cfparam name="args.classification" />
<cfparam name="args.designation" />
<cfparam name="args.average_height" />
<cfparam name="args.average_lifespan" />
<cfparam name="args.eye_colors" />
<cfparam name="args.hair_colors" />
<cfparam name="args.skin_colors" />
<cfparam name="args.language" />
<cfparam name="args.homeworldId" />
<cfparam name="args.homeworldName" />

<cfoutput>
    <h2>#args.name#</h2>

    <dl>
        <dt>Classification</dt>
        <dd>#args.classification#</dd>
        <dt>Designation</dt>
        <dd>#args.designation#</dd>
        <cfif isNumeric( args.average_height ) and args.average_height gt 0>
            <dt>Average height</dt>
            <dd>#args.average_height# centimeters</dd>
        </cfif>
        <cfif isNumeric( args.average_lifespan ) and args.average_lifespan gt 0>
            <dt>Average lifespan</dt>
            <dd>#args.average_lifespan# years</dd>
        </cfif>
        <dt>Eye colors</dt>
        <dd>#args.eye_colors#</dd>
        <dt>Hair colors</dt>
        <dd>#args.hair_colors#</dd>
        <dt>Skin colors</dt>
        <dd>#args.skin_colors#</dd>
        <dt>Language</dt>
        <dd>#args.language#</dd>
        <dt>Homeworld</dt>
        <dd><a href="?display=planet&id=#args.homeworldId#">#args.homeworldName#</a></dd>
    </dl>
</cfoutput>