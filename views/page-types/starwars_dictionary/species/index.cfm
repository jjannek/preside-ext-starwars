<cfparam name="args.name" />
<cfparam name="args.classification" />
<cfparam name="args.designation" />
<cfparam name="args.average_height" />
<cfparam name="args.average_lifespan" />
<cfparam name="args.eye_colors" />
<cfparam name="args.hair_colors" />
<cfparam name="args.skin_colors" />
<cfparam name="args.language" />
<cfparam name="args.homeworldId" />
<cfparam name="args.homeworldName" />
<cfparam name="args.people" />
<cfparam name="args.films" />

<cfoutput>
    #renderView( view="page-types/starwars_dictionary/species/_heading" , args=args )#

    #renderView( view="page-types/starwars_dictionary/character/listing", args={ characters = args.people, heading="h3", title="People" } )#
    #renderView( view="page-types/starwars_dictionary/film/listing"     , args={ films      = args.films , heading="h3" } )#
</cfoutput>